var mangNhanVien = [];
document.getElementById("btnThemNV").onclick = function () {
  var nhanVien = new NhanVien();
  nhanVien.taiKhoan = document.getElementById("tknv").value;
  nhanVien.hoTen = document.getElementById("name").value;
  nhanVien.email = document.getElementById("email").value;
  nhanVien.mk = document.getElementById("password").value;
  nhanVien.date = document.getElementById("datepicker").value;
  nhanVien.luongCoBan = document.getElementById("luongCB").value * 1;
  nhanVien.chucVu = document.getElementById("chucvu").value;
  nhanVien.gioLam = document.getElementById("gioLam").value;
  console.log("hello", nhanVien);
  // thực hiện validation
  var valid = true;
  kiemTraRong(nhanVien.taiKhoan, "#tbTKNV", "Tài khoản") &
    kiemTraRong(nhanVien.hoTen, "#tbTen", "Họ và tên") &
    kiemTraRong(nhanVien.email, "#tbEmail", "Email") &
    kiemTraRong(nhanVien.mk, "#tbMatKhau", "Mật khẩu") &
    kiemTraRong(nhanVien.date, "#tbNgay", "Ngày") &
    kiemTraRong(nhanVien.luongCoBan, "#tbLuongCB", "Lương cơ bản") &
    kiemTraRongChucVu(nhanVien.chucVu, "#tbChucVu", "Chức vụ") &
    kiemTraRong(nhanVien.gioLam, "#tbGiolam", "Giờ làm");
  valid &= kiemTraDoDai(nhanVien.taiKhoan, "#kTTK", "Tài khoản", 4, 6);
  valid &= kiemTraKyTu(nhanVien.hoTen, "#kTTen", "Họ và tên");
  valid &= kiemTraEmail(nhanVien.email, "#kTEmail", "Email");
  valid &= kiemTraDoDai(nhanVien.mk, "#kTDoDaiMK", "Mật khẩu", 6, 10);
  valid &= kiemTraMK(nhanVien.mk, "#kTMK", "Mật khẩu");
  valid &=
    kiemTraMinMax(
      nhanVien.luongCoBan,
      "#kTLuongCB",
      "Lương cơ bản ",
      1000000,
      20000000
    ) & kiemTraMinMax(nhanVien.gioLam, "#kTGioLam", "Giờ làm ", 80, 200);

  valid &= kiemTraMaNhanVien(mangNhanVien, nhanVien.taiKhoan);
  if (!valid) return;
  // kết thúc validation
  mangNhanVien.push(nhanVien);
  setStorage(mangNhanVien, "mangNhanVien");
  var html = renderNhanVien(mangNhanVien);
  document.querySelector("tbody").innerHTML = html;
};
function renderNhanVien(arrNhanVien) {
  var html = "";
  var arrNhanVienLength = arrNhanVien.length;
  for (var i = 0; i < arrNhanVienLength; i++) {
    var nv = arrNhanVien[i];
    html += ` <tr>
        <td>${nv.taiKhoan}</td>
        <td>${nv.hoTen}</td>
        <td>${nv.email}</td>
        <td>${nv.date}</td>
        <td>${nv.chucVu}</td>
        <td>${tinhLuong(nv.chucVu, nv.luongCoBan)}</td>
        <td>${xepLoaiNV(nv.gioLam)}</td>
        <td>
        <btn class="btn btn-success" onclick="suaNhanVien(${
          nv.taiKhoan
        })">Sửa</btn> 
        <btn class="btn btn-danger" onclick="xoaNhanVien(${
          nv.taiKhoan
        })">Xóa</btn> 
        </td>
      </tr>`;
  }
  return html;
}
function xoaNhanVien(taiKhoan) {
  var viTri = timKiemViTri(mangNhanVien, taiKhoan);
  if (viTri !== 1) {
    mangNhanVien.splice(viTri, 1);
    setStorage(mangNhanVien, "mangNhanVien");
    var html = renderNhanVien(mangNhanVien);
    document.querySelector("tbody").innerHTML = html;
  }
}
function suaNhanVien(taiKhoan) {
  document.getElementById("btnThem").click();

  var viTri = timKiemViTri(mangNhanVien, taiKhoan);
  if (viTri == -1) {
    return;
  }
  var nhanVien = mangNhanVien[viTri];
  document.getElementById("tknv").disabled = true;
  hienThiThongTin(nhanVien);
}
function capNhapNV() {
  document.getElementById("tknv").disabled = false;
  var nhanVien = new NhanVien();
  nhanVien.taiKhoan = document.getElementById("tknv").value;
  nhanVien.hoTen = document.getElementById("name").value;
  nhanVien.email = document.getElementById("email").value;
  nhanVien.mk = document.getElementById("password").value;
  nhanVien.date = document.getElementById("datepicker").value;
  nhanVien.luongCoBan = document.getElementById("luongCB").value * 1;
  nhanVien.chucVu = document.getElementById("chucvu").value;
  nhanVien.gioLam = document.getElementById("gioLam").value;
  var valid = true;
  kiemTraRong(nhanVien.taiKhoan, "#tbTKNV", "Tài khoản") &
    kiemTraRong(nhanVien.hoTen, "#tbTen", "Họ và tên") &
    kiemTraRong(nhanVien.email, "#tbEmail", "Email") &
    kiemTraRong(nhanVien.mk, "#tbMatKhau", "Mật khẩu") &
    kiemTraRong(nhanVien.date, "#tbNgay", "Ngày") &
    kiemTraRong(nhanVien.luongCoBan, "#tbLuongCB", "Lương cơ bản") &
    kiemTraRongChucVu(nhanVien.chucVu, "#tbChucVu", "Chức vụ") &
    kiemTraRong(nhanVien.gioLam, "#tbGiolam", "Giờ làm");
  valid &= kiemTraDoDai(nhanVien.taiKhoan, "#kTTK", "Tài khoản", 4, 6);
  valid &= kiemTraKyTu(nhanVien.hoTen, "#kTTen", "Họ và tên");
  valid &= kiemTraEmail(nhanVien.email, "#kTEmail", "Email");
  valid &= kiemTraDoDai(nhanVien.mk, "#kTDoDaiMK", "Mật khẩu", 6, 10);
  valid &= kiemTraMK(nhanVien.mk, "#kTMK", "Mật khẩu");
  valid &=
    kiemTraMinMax(
      nhanVien.luongCoBan,
      "#kTLuongCB",
      "Lương cơ bản ",
      1000000,
      20000000
    ) & kiemTraMinMax(nhanVien.gioLam, "#kTGioLam", "Giờ làm ", 80, 200);
  if (!valid) return;
  var viTri = timKiemViTri(mangNhanVien, nhanVien.taiKhoan);
  mangNhanVien[viTri] = nhanVien;
  var html = renderNhanVien(mangNhanVien);
  setStorage(mangNhanVien, "mangNhanVien");
  document.querySelector("tbody").innerHTML = html;
}
function timNhanVien() {
  var arrNV = "";
  mangNhanVienChild = [];

  var valueInput = document.getElementById("searchName").value;
  for (var i = 0; i < mangNhanVien.length; i++) {
    var xeploai = xepLoaiNV(mangNhanVien[i].gioLam);

    if (valueInput === xeploai) {
      arrNV = mangNhanVien[i];
      console.log("hee", arrNV);
      mangNhanVienChild.push(arrNV);
    }
  }
  if (arrNV !== "") {
    document.getElementById("tbPNV").innerHTML = "";
    var html = renderNhanVien(mangNhanVienChild);
    document.querySelector("tbody").innerHTML = html;
  } else {
    document.getElementById("tbPNV").innerHTML =
      "không tìm thấy loại nhân viên";
    var html = renderNhanVien(mangNhanVien);
    document.querySelector("tbody").innerHTML = html;
  }
}
function setStorage(content, storeName) {
  var sContent = JSON.stringify(content);
  localStorage.setItem(storeName, sContent);
}
function getStorage(storeName) {
  var output;
  if (localStorage.getItem(storeName)) {
    output = JSON.parse(localStorage.getItem(storeName));
  }
  return output;
}
window.onload = function () {
  var content = getStorage("mangNhanVien");
  if (content) {
    mangNhanVien = content;
    var html = renderNhanVien(content);
    document.querySelector("tbody").innerHTML = html;
  }
};
