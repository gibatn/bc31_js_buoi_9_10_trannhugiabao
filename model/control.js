function hienThiThongTin(nhanVien) {
  document.getElementById("tknv").value = nhanVien.taiKhoan;
  document.getElementById("name").value = nhanVien.hoTen;
  document.getElementById("email").value = nhanVien.email;
  document.getElementById("password").value = nhanVien.mk;
  document.getElementById("datepicker").value = nhanVien.date;
  document.getElementById("luongCB").value = nhanVien.luongCoBan * 1;
  document.getElementById("chucvu").value = nhanVien.chucVu;
  document.getElementById("gioLam").value = nhanVien.gioLam;
}
function tinhLuong(chucVu, luongCB) {
  switch (chucVu) {
    case "Sếp":
      return luongCB * 3;
    case "Trưởng phòng":
      return luongCB * 2;
    case "Nhân viên":
      return luongCB;
    default:
      return 0;
  }
}
function xepLoaiNV(gioLam) {
  if (gioLam < 160) {
    return "nhân viên trung bình";
  } else if (gioLam < 176) {
    return "nhân viên khá";
  } else if (gioLam < 192) {
    return "nhân viên giỏi";
  } else return "nhân viên xuất sắc";
}
function timKiemViTri(dsnv, taiKhoan) {
  return dsnv.findIndex(function (item) {
    return item.taiKhoan == taiKhoan;
  });
}
