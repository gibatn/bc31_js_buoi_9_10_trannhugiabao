function kiemTraRong(value, selectorError, name) {
  if (value == "") {
    document.querySelector(selectorError).innerHTML =
      name + " không được bỏ trống";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}
function kiemTraRongChucVu(value, selectorError, name) {
  if (value == 0) {
    document.querySelector(selectorError).innerHTML =
      name + " không được bỏ trống";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}
function kiemTraDoDai(value, selectorError, name, minValue, MaxValue) {
  if (value.length > MaxValue || value.length < minValue) {
    document.querySelector(selectorError).innerHTML =
      name + " có độ dài từ: " + minValue + " - " + MaxValue + " ký tự";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}
function kiemTraKyTu(value, selectorError, name) {
  var regexLetter = /^[A-Z a-z]+$/;
  if (regexLetter.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML = name + " phải là chữ cái";
  return false;
}
function kiemTraEmail(value, selectorError, name) {
  var regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (regexEmail.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML =
    name + " nhập chưa đúng định dạng </br> VD:example@email.com";
  return false;
}
function kiemTraMK(value, selectorError, name) {
  // var regexMK = /^(?=.*\d)(?=.*[a-z])(?=.*[!@#$%^&*]){6,10}$/;
  var regexMK = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/;
  if (regexMK.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML =
    name + " Bao gồm số,chữ in hoa, chữ thường và ký tự đặc biệt";
  return false;
}
function kiemTraMinMax(value, selecotrError, name, min, max) {
  if (value > max || value < min) {
    document.querySelector(selecotrError).innerHTML =
      name + "trong khoảng " + min + "-" + max;
    return false;
  }
  document.querySelector(selecotrError).innerHTML = "";
  return true;
}
function kiemTraMaNhanVien(mangNhanVien, taiKhoan) {
  var viTri = timKiemViTri(mangNhanVien, taiKhoan);
  if (viTri == -1) {
    document.getElementById("kTMaTK").innerHTML = "";
    return true;
  }
  document.getElementById("kTMaTK").innerHTML = "Tài khoản nhân viên bị trùng";
  return false;
}
